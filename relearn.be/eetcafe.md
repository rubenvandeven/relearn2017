## Relearn Rotterdam hosted by the Poortgebouw

Relearn Rotterdam is the 5th edition of a collective learning experiment, with as many teachers and it has participants. The event has grown from an interest in Free / Libre Open Source Software culture and practices as a way to address and acknowledge the production processes and frameworks involving technology and culture. The general focus of this year's Relearn is to reconsider forms of self-organising in regard to both machinic systems and social formations. We started to use the term "co-autonomies" to refer to these ideas. Over a period of five days, a group of interested artists, designers and activists will be looking to reconstruct notions of agency and consider ways of maintaining them in the form of four theme-based work tracks.

We invite you to join us on the 29th of August in Poortgebouw in a discussion about Relearn, its nomadic roots and its current home in this formerly squatted historical monument of Rotterdam. 

As part of this evening, the Poortgebouw residents will host an Eetcafé culinary evening, where vegan dinner will be served on donation. 

Next to this, Giulia de Giovanelli and Max Franklin will be presenting their work, The Autonomous Archive, a digital archivation system for Poortgebouw's historical documents.

* 19:00 Eetcafé, dinner & dessert
* 20:30 Relearn introduction
* 20:45 presentation of The Autonomous Archive

Join us on Tuesday to get to know the Poortgebouw and to find out more about Relearn.

<u>Poortgebouw</u>
Stieltjesstraat 38, Rotterdam
Tuesday 29th of August, 2017
19:00u - 21:00h
Ring the bel (there will be a bel)!

<u>Links</u>
[www.relearn.be](www.relearn.be/2017/)
[The Autonomous Archive](https://pad.pzimediadesign.nl/p/theautonomousarchive?showControls=false&showChat=false&showLineNumbers=false)


## Relearn Rotterdam gehost door het Poortgebouw

Relearn Rotterdam is de 5e editie van een collectief leer experiment, met net zoveel leraren als deelnemers. Het evenement is ontstaan vanuit een interesse in Vrije en Open Source Software-cultuur als een manier om de productieprocessen en -kaders van technologie en cultuur te ondervragen. De algemene focus van Relearn's editie dit jaar reageert op vormen van zelf-organisatie met betrekking tot zowel machinale systemen als sociale formaties. We hebben de term "co-autonomieën" geintroduceerd om naar deze ideëen te refereren. Een groep kunstenaars, ontwerpers en activisten zullen tijdens de week werken aan vier thematische werk-tracks, om samen ideeën rondom agentschap te reconstrueren en manieren te verkennen om deze te onderhouden op de lange termijn.

Wij nodigen je uit om op 29 augustus in Poortgebouw deel te namen aan een discussie over Relearn, zijn nomadische wortels en zijn huidige woning in dit voorheen gekraakte historische monument in Rotterdam. 

Als deel van deze avond zullen de bewoners van het Poortgebouw een Eetcafé organiseren, een culinaire avond waar een veganistisch diner wordt geserveerd tegen een kleine vergoeding.

Daarnaast presenteren Giulia de Giovanelli en Max Franklin hun werk, The Autonomous Archive, een digitaal archiveringssysteem voor historische documenten van het Poortgebouw.

* 19:00 Eetcafé, diner en dessert op donatie
* 20:30 Herleiden introductie
* 20:45 presentatie van het autonome archief

Doe mee met ons op dinsdagavond om het Poortgebouw te leren kennen en meer te weten te komen over Relearn.

<u>Poortgebouw</u>
Stieltjesstraat 38, Rotterdam
Dinsdag 29 augustus, 2017
19:00u - 21:00h
Bel aan (er zal een deurbel zijn)!

<u>Links</u>
[www.relearn.be](www.relearn.be/2017/)
[The Autonomous Archive](https://pad.pzimediadesign.nl/p/theautonomousarchive?showControls=false&showChat=false&showLineNumbers=false)
