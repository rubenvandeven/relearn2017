	 ____      _                         ____   ___  _ _____ 
	|  _ \ ___| | ___  __ _ _ __ _ __   |___ \ / _ \/ |___  |
	| |_) / _ \ |/ _ \/ _` | '__| '_ \    __) | | | | |  / / 
	|  _ <  __/ |  __/ (_| | |  | | | |  / __/| |_| | | / /
	|_| \_\___|_|\___|\__,_|_|  |_| |_| |_____|\___/|_|/_/


[www.relearn.be/2017/](www.relearn.be/2017/)

The bare git repository is hosted at [https://gitlab.com/relearn/relearn2017](https://gitlab.com/relearn/relearn2017).

Note: There is currently no direct link between this repository and the OSP server where the website is hosted.
