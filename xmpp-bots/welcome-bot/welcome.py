#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# le RMS XMPP bot
# usage: relearn.py [-h] -j JID -p PASSWORD -m MUC
# for example: $ python relearn.py -j username@domain.org -p entrepot -m room@muc.domain.org

# before running this script: 
# $ pip install argparse logging sleekxmpp pyasn1 pyasn1_modules
import argparse, json, logging, sleekxmpp


# command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-j", "--jid", help="jabber identifier", type=str, required=True)
parser.add_argument("-p", "--password", help="password", type=str, required=True)
parser.add_argument("-m", "--muc", help="destination muc", type=str, required=True)
args = parser.parse_args()

# data
with open('relearn.json') as data_file:    
	data = json.load(data_file)


# la Relearn bot
class Relearn(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		self.room = room
		self.nick = nick

		self.add_event_handler("session_start", self.start) 
		self.add_event_handler("groupchat_message", self.relearn_functions)
		self.add_event_handler("muc::%s::got_online" % self.room, self.welcome)


	def start(self, event):
		self.get_roster()
		self.send_presence()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick)

		# XEP-0084 User Avatar 
		# Requires SleekXMPP 81b7b2c1908e0f6a5435ce67745b5f4dafb59816

		with open('pg.jpg', 'rb') as avatar_file:
			avatar = avatar_file.read()
		avatar_id = self['xep_0084'].generate_id(avatar)
		info = {
			'id': avatar_id,
			'type': 'image/jpeg',
			'bytes': len(avatar)
			}
		self['xep_0084'].publish_avatar(avatar)
		self['xep_0084'].publish_avatar_metadata(items=[info])

		# XEP-0153: vCard-Based Avatars
		# Not working ATM

		#self['xep_0153'].set_avatar(avatar=avatar, mtype='image/png')

	def welcome(self, presence):
		if presence['muc']['nick'] != self.nick:
			self.send_message(mto=presence['from'].bare,
				mbody="Hello %s, welcome to Relearn! Do you like being a %s?" % (presence['muc']['nick'],
					presence['muc']['role']),
				mtype='groupchat')

	def relearn_functions(self, msg):
		for comment in data:
			triggers = comment['triggers']
			if msg['mucnick'] != self.nick and any(x in msg['body'].lower() for x in triggers):
				self.send_message(mto=msg['from'].bare,
					mbody=comment['response'], mtype='groupchat')

if __name__ == '__main__':

	logging.basicConfig(level=logging.INFO,
		format='%(levelname)-8s %(message)s')

	client = Relearn(args.jid, args.password, args.muc, "Relearn")

	client.register_plugin('xep_0045')
	client.register_plugin('xep_0030')
	client.register_plugin('xep_0084')
	#client.register_plugin('xep_0153')

	if client.connect():
		client.process(block=True)
	else:
		print("Can't connect.")


