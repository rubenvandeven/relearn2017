import datetime, os
import markdown
from weasyprint import HTML, CSS

msg={}
msg['mucnick'] = 'test'

# xmpp message
message = 'This is a **test** message, it *will* be printed.'
timestamp = datetime.datetime.now().strftime("%y-%m-%d-%H-%M-%S")
filename = 'update_message_'+timestamp
stringtime = datetime.datetime.now().strftime("%d %m %y, at %H:%Mh")

# markdown > html
html = markdown.markdown(message)

# adding a footer
footer = '<div id="footer">Sent to the printer by '+msg['mucnick']+' on '+stringtime+'.</div>'
html = html + footer

# html > pdf, with Weasyprint
HTML(string=html).write_pdf('pdf/'+filename+'.pdf', stylesheets=[CSS('css/stylesheet.css')])

# trigger the printer
os.system('cat pdf/'+filename+'.pdf | lpr')
print '*document printed*'