#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# le RMS XMPP bot
# usage: relearn.py [-h] -j JID -p PASSWORD -m MUC
# for example: $ python relearn.py -j username@domain.org -p entrepot -m room@muc.domain.org

# before running this script:
# $ pip install argparse logging sleekxmpp pyasn1 pyasn1_modules
import argparse, json, logging, sleekxmpp
import datetime, os
import markdown
from weasyprint import HTML, CSS

# command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-j", "--jid", help="jabber identifier", type=str, required=True)
parser.add_argument("-p", "--password", help="password", type=str, required=True)
parser.add_argument("-m", "--muc", help="destination muc", type=str, required=True)
args = parser.parse_args()

with open('commands.json') as data_file:
	data = json.load(data_file)
# ca_certs = '/etc/ssl/certs/ssl-cert-snakeoil.pem'

# la Relearn tool introduction bot
class Relearn(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		self.room = room
		self.nick = nick

		self.add_event_handler("session_start", self.start)
		self.add_event_handler("groupchat_message", self.print_now)
		self.add_event_handler("muc::%s::got_online" % self.room, self.welcome)

	def start(self, event):
		self.get_roster()
		self.send_presence()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick)

		# XEP-0084 User Avatar
		# Requires SleekXMPP 81b7b2c1908e0f6a5435ce67745b5f4dafb59816

		with open('pg.jpg', 'rb') as avatar_file:
			avatar = avatar_file.read()
		avatar_id = self['xep_0084'].generate_id(avatar)
		info = {
			'id': avatar_id,
			'type': 'image/jpeg',
			'bytes': len(avatar)
			}
		self['xep_0084'].publish_avatar(avatar)
		self['xep_0084'].publish_avatar_metadata(items=[info])

		# XEP-0153: vCard-Based Avatars
		# Not working ATM

		#self['xep_0153'].set_avatar(avatar=avatar, mtype='image/png')

	def welcome(self, presence):
		if presence['muc']['nick'] != self.nick:
			self.send_message(mto=presence['from'].bare,
				mbody="Hello %s, welcome to this collection muc called '%s'. Any message to this groupchat will be printed." % (presence['muc']['nick'], self.room.replace('@muc.complex.local','')),
				mtype='groupchat')

	def print_now(self, msg):
		if msg['body'] != '':

			except = ['welcome to this collection muc called', 'Thanks, i will print that for you.']
			if not any(x in msg['body'] for x in except):

				timestamp = datetime.datetime.now().strftime("%y-%m-%d-%H-%M-%S")
				filename = 'update_message_'+timestamp
				stringtime = datetime.datetime.now().strftime("%d %m %y, at %H:%Mh")

				# xmpp message
				message = msg['body']

				# markdown > html
				html = markdown.markdown(message)

				# adding a header
				header = '<div id="header">Latest message from updates@muc.complex.local:</div>'
				html = header + html

				# adding a footer
				footer = '<div id="footer">Sent to the printer by '+msg['mucnick']+' on '+stringtime+'.</div>'
				html = html + footer

				# html > pdf, with Weasyprint
				HTML(string=html).write_pdf('pdf/'+filename+'.pdf', stylesheets=[CSS('css/stylesheet.css')])

				# trigger the printer
				os.system('cat pdf/'+filename+'.pdf | lpr -P HP_Color_LaserJet_8550 -o portrait')
				print '*document printed*'

				# response of the bot to the muc
				if msg['mucnick'] != self.nick:
					self.send_message(mto=msg['from'].bare,
						mbody='Thanks, i will print that for you.', mtype='groupchat')

if __name__ == '__main__':

	logging.basicConfig(level=logging.INFO,
		format='%(levelname)-8s %(message)s')

	client = Relearn(args.jid, args.password, args.muc, "bot")

	client.register_plugin('xep_0045')
	client.register_plugin('xep_0030')
	client.register_plugin('xep_0084')
	#client.register_plugin('xep_0153')

	if client.connect():
		client.process(block=True)
	else:
		print("Can't connect.")
